#!/bin/bash

plasma_version="$1"
tmp_dir="tmp"

mkdir -p "$tmp_dir"
mkdir -p applets/digital-clock-fa/package
mkdir -p applets/mediacontroller-fa/package
mkdir -p applets/pager-fa/package

git_projects=( "plasma-workspace" "plasma-desktop" "kdeplasma-addons" )
for git_project in "${git_projects[@]}"; do
    git -C "$tmp_dir/$git_project" pull origin "$plasma_version" || git -C "$tmp_dir" clone --depth 1 --branch $plasma_version git@invent.kde.org:plasma/$git_project.git
done

cp -r "$tmp_dir/plasma-workspace/applets/digital-clock/package/"* applets/digital-clock-fa/package/
cp -r "$tmp_dir/plasma-workspace/applets/mediacontroller/package/"* applets/mediacontroller-fa/package/
cp -r "$tmp_dir/plasma-desktop/applets/pager/package/"* applets/pager-fa/package/

# com.gitlab.skiron76.digital-clock-fa
